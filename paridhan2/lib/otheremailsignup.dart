import 'package:flutter/material.dart';
import 'package:paridhan2/verifyemailsignup.dart';

class Esignup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red[800],
          title: Text('Welcome to Paridhan Samadhan'),
        ),
        body: Column(
          children: [
            TextField(
              decoration: InputDecoration(labelText: 'Enter Your Name'),
            ),
            TextField(
              decoration: InputDecoration(labelText: 'Enter your email id'),
            ),
            TextField(
              decoration: InputDecoration(labelText: 'Enter Your Password'),
            ),
            TextField(
              decoration: InputDecoration(labelText: 'Re enter Your Password'),
            ),
            ElevatedButton(
                child: Text(
                  'Verify email ',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => VerifySignup()),
                  );
                }),
            ElevatedButton(
                child: Text(
                  'Back',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                })
          ],
        ),
        backgroundColor: Colors.lime,
      ),
    );
  }
}
