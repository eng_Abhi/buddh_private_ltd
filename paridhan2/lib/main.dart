import 'package:flutter/material.dart';
import 'package:paridhan2/otheremail.dart';
import './mobile.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyButtonArray(),
    );
  }
}

class MyButtonArray extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red[800],
        title: Text('Welcome to Paridhan Samadhan'),
      ),
      body: Column(
        children: [
          Text(
            'Kindly Login to continue..',
            style: TextStyle(fontSize: 30),
          ),
          ElevatedButton(
              child: Text(
                'Login using Mobile Number',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Mobile()),
                );
              }
              //print('Login using Mobile choosen'),
              ),
          ElevatedButton(
            child: Text(
              'Login using Facebook',
              style: TextStyle(
                color: Colors.black,
                fontSize: 20,
              ),
            ),
            onPressed: () => print('Login using facebook choosen'),
          ),
          ElevatedButton(
            child: Text(
              'Login using Google',
              style: TextStyle(
                color: Colors.black,
                fontSize: 20,
              ),
            ),
            onPressed: () => print('Login using Google choosen'),
          ),
          Text(
            '         Or     ',
            style: TextStyle(
              color: Colors.red[900],
              fontSize: 20,
            ),
          ),
          ElevatedButton(
              child: Text(
                'Login using other email: ',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => OtherEmail()),
                );
              }),
          Text('  '),
          Text(' '),
          Text(
            'Thank you for choosing Paridhan Samadhan. we are here to solve your complete dressing problem.. You can either buy desired dresses or take it on rent. we endeavor to deliever you dresses at the lowest price/rent.',
            style: TextStyle(
              color: Colors.black,
              fontSize: 22,
            ),
          ),
        ],
      ),
      backgroundColor: Colors.lime,
    );
  }
}
