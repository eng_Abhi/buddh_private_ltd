import 'package:flutter/material.dart';
import 'package:paridhan2/SearchResult.dart';

//void main() {
//runApp(Buyer());
//}

class Buyer extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyBuyer(),
    );
  }
}

class MyBuyer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red[800],
        title: Text('Welcome to Paridhan Samadhan'),
      ),
      body: Column(
        children: [
          TextField(
            decoration:
                InputDecoration(labelText: "Search by product name/age/gender"),
          ),
          ElevatedButton(
              child: Text(
                'Search',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SearchResult()),
                );
              }),
              ElevatedButton(
                child: Text(
                  'Back',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                })
        ],
      ),
      backgroundColor: Colors.lime,
    );
  }
}
