import 'package:flutter/material.dart';

class Otp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red[800],
          title: Text('Welcome to Paridhan Samadhan'),
        ),
        body: Column(
          children: [
            TextField(
              decoration:
                  InputDecoration(labelText: 'Enter OTP sent to your Mobile'),
            ),
            ElevatedButton(
              child: Text(
                'Resend OTP',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
              onPressed: () => print('Login using Mobile choosen'),
            ),
            ElevatedButton(
              child: Text(
                'Verify OTP',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
              onPressed: () => print('Login using Mobile choosen'),
            ),
            ElevatedButton(
                child: Text(
                  'Back',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                })
          ],
        ),
        backgroundColor: Colors.lime,
      ),
    );
  }
}
