import 'package:flutter/material.dart';
import 'package:paridhan2/Mobileotp.dart';

class Mobile extends StatelessWidget {
  // goBackToPreviousScreen(BuildContext context) {
  //   Navigator.pop(context);
  // }

  @override
  Widget build(BuildContext context) {
    // return MaterialApp(
    // home:
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red[800],
        title: Text('Welcome to Paridhan Samadhan'),
      ),
      body: Column(
        children: [
          TextField(
            decoration: InputDecoration(labelText: 'Enter mobile number'),
            // style: TextStyle(backgroundColor: Colors.white),
          ),
          ElevatedButton(
              child: Text(
                'Generate OTP ',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Otp()),
                );
              }),
          ElevatedButton(
              child: Text(
                'Back',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
              onPressed: () {
                Navigator.pop(context);
              })
        ],
      ),
      backgroundColor: Colors.lime,
      //),
    );
  }
}
