import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text("Welcome to Paridhan Samadhan"),
          ),
          body: SafeArea(
              child: Center(
            child: DropButton(),
          ))),
    );
  }
}

class DropButton extends StatefulWidget {
  @override
  _DropButtonState createState() => _DropButtonState();
}

class _DropButtonState extends State<DropButton> {
  String dropdownValueGd = 'Women';
  String dropdownValueCt = 'Lehenga';
  String dropdownValueSz = 'S';
  Widget dropDownCt;
  Widget dropDownSz;

  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        TextField(
          decoration: InputDecoration(labelText: 'Image Location'),
        ),
        ElevatedButton(
            child: Text(
              ' Browse ',
              style: TextStyle(
                color: Colors.black,
                fontSize: 18,
              ),
            ),
            onPressed: () {
              print("Browse option chooses");
            }),
        TextField(
          decoration: InputDecoration(labelText: 'Enter Price of sell'),
        ),
        TextField(
          decoration: InputDecoration(labelText: 'Enter Price on Rent per day'),
        ),
        DropdownButton<String>(
          value: dropdownValueGd,
          icon: Icon(Icons.arrow_downward),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.deepPurple),
          underline: Container(
            height: 2,
            color: Colors.deepPurpleAccent,
          ),
          onChanged: (String newValue) {
            setState(() {
              dropdownValueGd = newValue;
              _uploadCategory(newValue);
            });
          },
          items: <String>['Women', 'Girls']
              .map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
        if (null != dropDownCt) dropDownCt,
        if (null != dropDownSz) dropDownSz,
        ElevatedButton(
            child: Text(
              'Upload',
              style: TextStyle(
                color: Colors.black,
                fontSize: 20,
              ),
            ),
            onPressed: () {
              print("Upload button choosen");
            }),
      ],
    );
  }

  _uploadCategory(String gd) {
    List<String> itemCtArray;

    if (gd == 'Women') {
      itemCtArray = ['Saree', 'Lehenga', 'Western Dresses'];
    } else {
      itemCtArray = ['Frock', 'Lehenga'];
    }
    dropDownCt = DropdownButton<String>(
      value: dropdownValueCt,
      icon: Icon(Icons.arrow_downward),
      iconSize: 24,
      elevation: 16,
      style: TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (String newValue) {
        setState(() {
          dropdownValueCt = newValue;
          _uploadSize(newValue);
        });
      },
      items: itemCtArray.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }

  _uploadSize(String ct) {
    List<String> itemSzArray;

    switch (ct) {
      case 'Lehenga':
        itemSzArray = ['S', 'M', 'XL', 'XXL'];
        break;
      case 'Western Dresses':
        itemSzArray = ['S', 'M', 'XL', 'XXL'];
        break;
      case 'Frock':
        itemSzArray = ['S', 'M'];
        break;
    }
    dropDownSz = DropdownButton<String>(
      value: dropdownValueSz,
      icon: Icon(Icons.arrow_downward),
      iconSize: 24,
      elevation: 16,
      style: TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (String newValue) {
        setState(() {
          dropdownValueSz = newValue;
        });
      },
      items: itemSzArray.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
