import 'package:flutter/material.dart';

//void main() {
//  runApp(SearchResult());
//}

class SearchResult extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red[800],
          title: Text('Welcome to Paridhan Samadhan'),
        ),
        body: Row(
          children: [
            ElevatedButton(
              child: Text(
                'Buy',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
              onPressed: () => print('Buy option choosen'),
            ),
            ElevatedButton(
              child: Text(
                'Take on Rent',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
              onPressed: () => print('Rent option choosen'),
            ),
            ElevatedButton(
                child: Text(
                  'Back',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                })
          ],
        ),
      ),
    );
  }
}
