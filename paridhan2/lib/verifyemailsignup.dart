import 'package:flutter/material.dart';

class VerifySignup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red[800],
          title: Text('Welcome to Paridhan Samadhan'),
        ),
        body: Column(
          children: [
            TextField(
              decoration: InputDecoration(
                  labelText: 'Enter Verification code sent to your email'),
            ),
            ElevatedButton(
              child: Text(
                '  Verify  ',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
              onPressed: () => print('Login Other email Signup choosen'),
            ),
            ElevatedButton(
              child: Text(
                '  Resend  ',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
              onPressed: () => print('Login Other email Signup choosen'),
            ),
            ElevatedButton(
                child: Text(
                  'Back',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                })
          ],
        ),
        backgroundColor: Colors.lime,
      ),
    );
  }
}
