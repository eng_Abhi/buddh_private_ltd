import 'package:flutter/material.dart';
import 'package:paridhan2/otheremailsignup.dart';

class OtherEmail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red[800],
          title: Text('Welcome to Paridhan Samadhan'),
        ),
        body: Column(
          children: [
            TextField(
              decoration: InputDecoration(labelText: 'Enter Email id'),
            ),
            TextField(
              decoration: InputDecoration(labelText: 'Enter Password'),
            ),
            ElevatedButton(
              child: Text(
                'Login',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
              onPressed: () => print('Login using Other email choosen'),
            ),
            ElevatedButton(
              child: Text(
                'Forgot Password',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
              onPressed: () => print('Login using Other email choosen'),
            ),
            Text(
              '         Or     ',
              style: TextStyle(
                color: Colors.red[900],
                fontSize: 20,
              ),
            ),
            ElevatedButton(
                child: Text(
                  'Sign Up',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Esignup()),
                  );
                }),
            ElevatedButton(
                child: Text(
                  'Back',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                })
          ],
        ),
        backgroundColor: Colors.lime,
      ),
    );
  }
}
